const x = require('x-ray')();
const query = require('./query.js')
const request = require('request')
const uniqid = require('uniqid')
const SummaryTool = require('node-summary');
const decode = require('unescape');

function param(uri, obj) {
    let str = uri + '?';
    for (let key in obj) {
        if (Array.isArray(obj[key])) {
            obj[key].forEach(x => {
                if (str != "") {
                    str += '&'
                }
                str += key + "=" + encodeURIComponent(x)
            })
        } else {
            if (str != "") {
                str += '&'
            }
            str += key + "=" + encodeURIComponent(obj[key]);
        }
    }
    return str
}

function submitContent(payload) {
    payload.content.forEach((x, i) => {
        // console.log(x)
        if (i == 0) {
            query.article({
                uniqid: payload.unique,
                title: decode(x),
                image: payload.image,
                link: payload.link,
                lang: payload.lang,
                source: payload.author
            })
        } else {
            if (x || x.length > 0) {
                query.paragraph({
                    uniqid: payload.unique,
                    text: decode(x),
                    rank: i
                })
            }
        }
    })
}

function submitSummary(payload) {
    SummaryTool.getSortedSentences(payload.content, 6, function (err, summary) {
        if (err) return console.log("Something went wrong man!");
        summary.forEach((x, i) => {
            query.summary({
                uniqid: payload.unique,
                summary: decode(x),
                rank: i + 1
            })
        })
    })
}

function translate(payload) {
    let endpoint = 'https://translation.googleapis.com/language/translate/v2'
    let google_api_key = 'AIzaSyBrbZSPdlooj854k425AMK8XoSeStxZwMo'
    let qs = {
        'key': google_api_key,
        'q': payload.text[payload.index],
        'target': 'id'
    }
    // console.log(qs)
    let uri = param(endpoint, qs)

    request({
        uri: uri,
        method: 'GET',
    }, (error, response, body) => {
        // console.log(body)
        let data = JSON.parse(body)
        // console.log(data)

        if (payload.index === 0) {
            translate({
                author: payload.author,
                content: data.data.translations.map(x => x.translatedText),
                listing: payload.listing,
                callback: payload.callback,
                text: payload.text,
                link: payload.link,
                index: 1
            })
        } else if (payload.index === 1) {            
            const unique = uniqid()

            let content = payload.content.concat(data.data.translations.map(x => x.translatedText))
            submitContent({
                author: payload.author,
                content: content,
                unique: unique,
                image: payload.listing.image,
                link: payload.link,
                lang: 'ID'
            })

            let summary = content.join('\n')
            submitSummary({
                content: summary,
                unique: unique
            })

            payload.callback()
        }
    })
}

const get = (payload, callback) => {    
        x(payload.source, payload.target, [payload.model])
        .then(function (res) {            
            payload.middleware(res, (listing) => {
                //Escape if Article not Found
                if(listing[0] === undefined){
                    callback()
                    return
                }
                if(listing[0].title === undefined || listing[0].image === undefined){
                    callback()
                    return
                }

                listing = listing[0]
                // console.log(listing.title)
                // console.log(listing.body)
                let q = [listing.title].concat(listing.body)
                const unique = uniqid()
                
                submitContent({
                    author: payload.author,
                    content: q,
                    unique: unique,
                    image: listing.image,
                    link: payload.source,
                    lang: 'EN'
                })

                let content = listing.body.join('\n')
                submitSummary({
                    content: content,
                    unique: unique
                })


                // console.log(uri)
                // Hide Auto Translate cus its expensive
                // let text = [q.slice(0, Math.floor(q.length / 2)), q.slice(Math.floor(q.length / 2), q.length)]
                // translate({
                //     author: payload.author,
                //     link: payload.source,
                //     listing: listing,
                //     callback: callback,
                //     text: text,
                //     index: 0,                    
                // })
            })
        })
        .catch(function (err) {
            console.log(err) // handle error in promise
        })
}

module.exports = {
    get: get
}