const getPosts = require('./getPosts.js')

let library = {
    coindesk: {
        author: 'coindesk',
        post: {
            source: 'https://coindesk.com/',
            target: 'div.article',
            model: {
                'title': 'h3 a.fade',
                'link': 'h3 a@href'
            },
            middleware: (data, next) => {
                next(data)
            }
        },
        content: {
            author: 'coindesk',
            target: 'div.single-post',
            model: {
                'title': 'h3.article-top-title',
                'body': ['div.article-content-container > p'],
                'date': 'time@datetime',
                'image': 'div.article-top-image-section@style'
            },
            middleware: (data, next) => {
                data[0]['image'] = String(data[0]['image']).replace("background-image:url(\"", "")
                data[0]['image'] = String(data[0]['image']).replace("\")", "")
                next(data)
            }
        }
    },
    cointelegraph: {
        author: 'cointelegraph',
        post: {
            source: 'https://cointelegraph.com/',
            target: 'div.post',
            model: {
                'title': 'span.postTitle',                
                'link': 'a@href'
            },
            middleware: (data, next) => {
                next(data)
            }
        },
        content: {
            author: 'cointelegraph',
            target: 'div.post-area',
            model: {
                'title': 'h1.header',
                'body': ['div.post-full-text > p'],
                'date': 'time@datetime',
                'image': 'div.image img@src'
            },
            middleware: (data, next) => {            
                next(data)
            }
        }
    },
    ethnews: {
        author: 'ethnews',
        post: {
            source: 'https://www.ethnews.com/',
            target: 'div.article-thumbnail',
            model: {
                'title': 'h2.article-thumbnail__info__title',                
                'link': 'h2.article-thumbnail__info__title a@href'
            },
            middleware: (data, next) => {                
                next(data)
            }
        },
        content: {
            author: 'ethnews',
            target: 'div.content',
            model: {
                'title': 'div.container h1',
                'body': ['div.article__content > p'],
                'date': 'time@datetime',
                'image': ['div.article-cover__inner > img@data-src']
            },
            middleware: (data, next) => {
                data[0].image = data[0].image[1]                
                next(data)
            }
        }
    },
    bitcoinist: {
        author: 'bitcoinist',
        post: {
            source: 'http://bitcoinist.com/',
            target: 'article.post',
            model: {
                'title': 'h2.title',
                'link': 'h2.title a@href'
            },
            middleware: (data, next) => {                                
                next(data)
            }
        },
        content: {
            author: 'bitcoinist',
            target: 'main.single-post',
            model: {
                'title': 'h2.title',
                'body': ['article.post > p'],
                'date': 'time@datetime',
                'image': 'div.post-header > div@style'
            },
            middleware: (data, next) => {                
                let img = data[0]['image']
                img = img.substring(img.indexOf("http")).replace("\') no-repeat center center\;", "")                
                data[0]['image'] = img                
                next(data)
            }
        }
    },
    cryptorecorder: {
        author: 'cryptorecorder',
        post: {
            source: 'https://cryptorecorder.com/news/',
            target: 'article.post',
            model: {
                'title': 'h2.entry-title',
                'link': 'h2.entry-title a@href'
            },
            middleware: (data, next) => {                                                
                next(data)
            }
        },
        content: {
            author: 'cryptorecorder',
            target: 'main.site-main',
            model: {
                'title': 'header.page-header h1',
                'body': ['section.content > p'],
                'date': 'time@datetime',
                'image': 'section.post-media img@src'
            },
            middleware: (data, next) => {                                                
                next(data)
            }
        }
    }
}

module.exports = function(source) {
    getPosts.initiate(library[source])
}