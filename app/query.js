// const bcrypt = require('bcrypt-nodejs');
const mysql = require('mysql');

function slugify(text) {
  return text.toString().toLowerCase()
    .replace(/\s+/g, '-') // Replace spaces with -
    .replace(/[^\w\-]+/g, '') // Remove all non-word chars
    .replace(/\-\-+/g, '-') // Replace multiple - with single -
    .replace(/^-+/, '') // Trim - from start of text
    .replace(/-+$/, ''); // Trim - from end of text
}

function devMode(x) {
  if (x === 'server') {
    return mysql.createPool({
      acquireTimeout: 100000,
      connectionLimit: 200,
      host: 'coinstory.c0sqiigeihke.ap-southeast-1.rds.amazonaws.com',
      user: 'coinstory',
      password: 'marcelc6363',
      database: 'coinstory'
    })
  }
  if (x === 'localhost') {
    return mysql.createPool({
      connectionLimit: 200,
      host: 'localhost',
      user: 'root',
      password: '',
      database: 'crypto'
    })
  }
}
const pool = devMode('server')

const callbackCheck = (callback, data) => {
  if (data === 400) {
    data = {
      meta: {
        code: 400,
      }
    }
  }
  if (callback !== undefined) {
    callback(data)
  }
}

const mysqlQuery = (sql, callback) => {
  pool.getConnection((err, connection) => {
    if (err) throw err;
    connection.query(sql, function (err, result, fields) {
      if (err) throw err;
      callback(result, fields)
      connection.release()
    });
  });
}

const article = (payload, callback) => {
  //Switch
  let sql = "INSERT INTO `article` (uniqid, title, slug, image, link, source, timestring, lang) VALUE (?, ?, ?, ?, ?, ?, ?, ?)"
  let timestring = Math.round((new Date()).getTime() / 1000)
  let slug = slugify(payload.title)

  console.log('slug', slug)

  //Query
  mysqlQuery({
    sql: sql,
    timeout: 40000, // 40s
    values: [payload.uniqid, payload.title, slug, payload.image, payload.link, payload.source, timestring, payload.lang]
  }, (x) => {
    if (x.length !== 0) {
      let data = {
        meta: {
          code: 200,
        }
      }
      callbackCheck(callback, data)
    }
  })
}

const paragraph = (payload, callback) => {
  //Switch
  let sql = "INSERT INTO `paragraph` (uniqid, text, rank) VALUE (?, ?, ?)"

  if (payload.text || payload.text.length > 0) {
    //Query
    mysqlQuery({
      sql: sql,
      timeout: 40000, // 40s
      values: [payload.uniqid, payload.text, payload.rank]
    }, (x) => {
      if (x.length !== 0) {
        let data = {
          meta: {
            code: 200,
          }
        }
        callbackCheck(callback, data)
      }
    })
  }
}

const summary = (payload, callback) => {
  //Switch
  let sql = "INSERT INTO `summary` (uniqid, summary, rank) VALUE (?, ?, ?)"

  //Query
  mysqlQuery({
    sql: sql,
    timeout: 40000, // 40s
    values: [payload.uniqid, payload.summary, payload.rank]
  }, (x) => {
    if (x.length !== 0) {
      let data = {
        meta: {
          code: 200,
        }
      }
      callbackCheck(callback, data)
    }
  })
}

const articleCheck = (payload, callback) => {
  //Switch
  let sql = "SELECT * FROM `article` WHERE `slug` = ?"

  //Query
  mysqlQuery({
    sql: sql,
    timeout: 40000, // 40s
    values: [payload.slug]
  }, (x) => {
    console.log('Check Article', x)

    if (x.length === 0) {
      let data = {
        meta: {
          code: 200,
        }
      }
      callbackCheck(callback, data)
    }
    if (x.length > 0) {
      let data = {
        meta: {
          code: 400,
        }
      }
      callbackCheck(callback, data)
    }
  })
}

const getArticle = (payload, callback) => {

  let page = payload.page * 10

  //Switch
  let sql = "SELECT * FROM `article` WHERE `lang` = ? GROUP BY `slug` ORDER BY `timestring` DESC LIMIT 10 OFFSET ?"
  console.log(payload)
  //Query
  mysqlQuery({
    sql: sql,
    timeout: 40000, // 40s
    values: [payload.lang, page]
  }, (x) => {
    if (x.length > 0) {
      let data = {
        meta: {
          code: 200,
        },
        data: x
      }
      callbackCheck(callback, data)
    }
    if (x.length === 0) {
      let data = {
        meta: {
          code: 400,
        }
      }
      callbackCheck(callback, data)
    }
  })
}

const getStoryOrder = (payload, callback) => {
  //Switch
  // let sql = "SELECT * FROM `article` WHERE `lang` = ? AND `source` = ? AND `timestring` >= ? ORDER BY `timestring` ASC"
  // let sql = "SELECT * FROM `author` RIGHT JOIN (SELECT * FROM `article` WHERE article.timestring > ? ORDER BY article.timestring DESC) as article on author.name = article.source WHERE article.lang = 'EN' GROUP BY article.source ORDER BY article.timestring DESC"
  let sql = "SELECT * FROM `author` LEFT JOIN (SELECT * FROM `article`WHERE article.timestring >= ? ORDER BY article.timestring DESC) as article on article.title LIKE CONCAT(author.name, '%') WHERE article.lang = 'EN' GROUP BY author.name ORDER BY article.timestring DESC"

  //Query
  mysqlQuery({
    sql: sql,
    timeout: 40000, // 40s
    values: [payload.timestring]
  }, (x) => {
    if (x.length > 0) {
      let data = {
        meta: {
          code: 200,
        },
        data: x
      }
      callbackCheck(callback, data)
    }
    if (x.length === 0) {
      let data = {
        meta: {
          code: 400,
        }
      }
      callbackCheck(callback, data)
    }
  })
}

const getStory = (payload, callback) => {
  //Switch
  // let sql = "SELECT * FROM `article` WHERE `lang` = ? AND `source` = ? AND `timestring` >= ? GROUP BY `slug` ORDER BY `timestring` ASC"

  let sql = "SELECT * FROM `article` WHERE `lang` = ? AND `title` LIKE ? AND `timestring` >= ? GROUP BY `slug` ORDER BY `timestring` ASC"

  //Query
  mysqlQuery({
    sql: sql,
    timeout: 40000, // 40s
    values: [payload.lang, payload.source+'%', payload.timestring]
  }, (x) => {
    if (x.length > 0) {
      let data = {
        meta: {
          code: 200,
        },
        data: x
      }
      callbackCheck(callback, data)
    }
    if (x.length === 0) {
      let data = {
        meta: {
          code: 400,
        }
      }
      callbackCheck(callback, data)
    }
  })
}

const getParagraph = (payload, callback) => {
  //Switch
  let sql = "SELECT * FROM `paragraph` WHERE `uniqid` = ? ORDER BY `rank` ASC"

  //Query
  mysqlQuery({
    sql: sql,
    timeout: 40000, // 40s
    values: [payload.uniqid]
  }, (x) => {
    if (x.length > 0) {
      let data = {
        meta: {
          code: 200,
        },
        data: x
      }
      callbackCheck(callback, data)
    }
    if (x.length === 0) {
      let data = {
        meta: {
          code: 400,
        }
      }
      callbackCheck(callback, data)
    }
  })
}

const getSummary = (payload, callback) => {
  //Switch
  let sql = "SELECT * FROM `summary` WHERE `uniqid` = ?"

  //Query
  mysqlQuery({
    sql: sql,
    timeout: 40000, // 40s
    values: [payload.uniqid]
  }, (x) => {
    if (x.length > 0) {
      let data = {
        meta: {
          code: 200,
        },
        data: x
      }
      callbackCheck(callback, data)
    }
    if (x.length === 0) {
      let data = {
        meta: {
          code: 400,
        }
      }
      callbackCheck(callback, data)
    }
  })
}

module.exports = {
  article: article,
  articleCheck: articleCheck,
  getArticle: getArticle,
  getStory: getStory,
  getParagraph: getParagraph,
  getSummary: getSummary,
  getStoryOrder: getStoryOrder,
  paragraph: paragraph,
  summary: summary
}