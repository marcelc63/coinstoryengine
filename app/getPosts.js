const x = require('x-ray')();
const query = require('./query.js')
const content = require('./getContent.js')

function slugify(text) {
    return text.toString().toLowerCase()
        .replace(/\s+/g, '-') // Replace spaces with -
        .replace(/[^\w\-]+/g, '') // Remove all non-word chars
        .replace(/\-\-+/g, '-') // Replace multiple - with single -
        .replace(/^-+/, '') // Trim - from start of text
        .replace(/-+$/, ''); // Trim - from end of text
}

function loop(payload, index, config) {    
    console.log(payload.length, index)    
    let store = payload[index]    
    console.log(store)
    query.articleCheck({
        title: store.title,
        slug: slugify(store.title)
    }, (x) => {
        function cont() {
            if (index < payload.length - 1) {
                loop(payload, index + 1, config)                
            } else {
                console.log('done')
            }
        }

        if (x.meta.code === 200) {
            content.get({
                author: config.author,
                source: store.link,
                target: config.target,
                model: config.model,
                middleware: config.middleware
            }, () => {
                cont()
            })
            console.log('unregistered')
        } else {
            cont()
            console.log('registered')
        }
    })
}

function initiate(payload) {    
    console.log('initiate')
    x(payload.post.source, payload.post.target, [payload.post.model])
        .then(function (res) {
            res = res.filter(x => x.title !== undefined && x.link !== undefined)
            payload.post.middleware(res, (store) => {
                loop(store, 0, payload.content)
            })            
        })
        .catch(function (err) {
            console.log(err) // handle error in promise
        })    
}

// initiate()

module.exports = {
    initiate: initiate
}