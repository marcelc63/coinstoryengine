const express = require("express");
const app = express();
const path = require("path");
const query = require("./app/query.js");
const getPosts = require("./app/getPosts.js");
const scrape = require("./app/scraper.js");
const schedule = require("node-schedule");

var j = schedule.scheduleJob("*/20 * * * *", function() {
  scrape("coindesk");
  scrape("cointelegraph");
  scrape("ethnews");
  scrape("bitcoinist");
  scrape("cryptorecorder");
});

function paragraph(x, callback) {
  query.getParagraph(
    {
      uniqid: x
    },
    t => {
      callback(t);
    }
  );
}

function summary(x, callback) {
  query.getSummary(
    {
      uniqid: x
    },
    t => {
      t.data = t.data.sort((a, b) => a.rank - b.rank);
      t.data.forEach(x => (x.type = "text"));
      callback(t);
    }
  );
}

app.use(express.static("public"));

app.get("/", function(req, res) {
  res.sendFile(path.join(__dirname + "/public/index.html"));
});

app.get("/scrape", function(req, res) {
  scrape(req.query.source);
});

app.get("/getParagraph", function(req, res) {
  query.getParagraph(
    {
      uniqid: req.query.uniqid
    },
    x => {
      if (x.meta.code === 200) {
        let message = {
          meta: {
            code: 200
          },
          data: x.data
        };
        res.setHeader("Content-Type", "application/json");
        res.send(JSON.stringify(message, null, 3));
      } else {
        let message = {
          meta: {
            code: 400
          }
        };
        res.setHeader("Content-Type", "application/json");
        res.send(JSON.stringify(message, null, 3));
      }
    }
  );
});

app.get("/getStoryOrder", function(req, res) {
  query.getStoryOrder(
    {
      timestring: Math.floor(new Date().getTime() / 1000) - 604800
    },
    x => {
      let message = x;
      res.setHeader("Content-Type", "application/json");
      res.send(JSON.stringify(message, null, 3));
    }
  );
});

app.get("/getStory", function(req, res) {
  let lang = req.query.lang === undefined ? "EN" : req.query.lang;

  query.getStory(
    {
      lang: lang,
      source: req.query.source,
      timestring: Math.floor(new Date().getTime() / 1000) - 604800
    },
    x => {
      if (x.meta.code === 200) {
        let message = {
          meta: {
            code: 200
          },
          data: x.data
        };
        res.setHeader("Content-Type", "application/json");
        res.send(JSON.stringify(message, null, 3));
      } else {
        let message = {
          meta: {
            code: 400
          }
        };
        res.setHeader("Content-Type", "application/json");
        res.send(JSON.stringify(message, null, 3));
      }
    }
  );
});

app.get("/getArticle", function(req, res) {
  let page = req.query.page === undefined ? 0 : req.query.page;
  let lang = req.query.lang === undefined ? "EN" : req.query.lang;

  query.getArticle(
    {
      lang: lang,
      page: page
    },
    x => {
      function process(store, index) {
        summary(store[index].uniqid, s => {
          let title = {
            uniqid: store[index].uniqid,
            title: store[index].title,
            type: "title"
          };
          let image = {
            uniqid: store[index].uniqid,
            image: store[index].image,
            title: store[index].title,
            type: "image"
          };

          store[index].content = s.data;
          store[index].content.unshift(image);

          if (index < store.length - 1) {
            process(store, index + 1);
          } else {
            let message = {
              meta: {
                code: 200
              },
              data: store
            };
            res.setHeader("Content-Type", "application/json");
            res.send(JSON.stringify(message, null, 3));
          }
        });
      }

      if (x.meta.code === 200) {
        process(x.data, 0);
      } else {
        let message = {
          meta: {
            code: 400
          }
        };
        res.setHeader("Content-Type", "application/json");
        res.send(JSON.stringify(message, null, 3));
      }
    }
  );
});

//Temporary
app.post("/post", function(req, res) {
  console.log(req.body);
  console.log(req.query);
  console.log(req.params);
  //   query.post(
  //     {
  //       title: req.body.title,
  //       story: req.body.story
  //     },
  //     () => {}
  //   );
});

app.listen("3000");

console.log("Magic happens on port 3000");

exports = module.exports = app;
